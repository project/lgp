<?php

define('LGP_LOG_FILENAME', 'lgp.log');

/**
 * Implements hook_boot().
 *
 * Declared just to make LGP available at bootstrap.
 */
function lgp_boot() {
}

/**
 * Implements hook_requirements().
 */
function lgp_requirements($phase) {
  $requirements = array();
  if ($phase == 'runtime') {
    $requirements['lgp'] = array(
      'title' => t('LGP log file'),
      'value' => lgp_temp_dir() . '/' .LGP_LOG_FILENAME,
      'severity' => REQUIREMENT_INFO,
      'weight' => -1000,
    );
  }
  return $requirements;
}

/**
 * Returns directory path used for temporary files.
 *
 * From http://www.php.net/manual/en/function.sys-get-temp-dir.php#94119
 */
function lgp_temp_dir() {
  foreach (array('TMP', 'TEMP', 'TMPDIR') as $var) {
    if ($temp = getenv($var)) {
      return realpath($temp);
    }
  }

  $temp = tempnam(__FILE__, '');
  if (file_exists($temp)) {
    unlink($temp);
    return realpath(dirname($temp));
  }

  return NULL;
}

function lgp_backtrace($ignore_args = TRUE) {
  return debug_backtrace($ignore_args ? DEBUG_BACKTRACE_IGNORE_ARGS : NULL);
}

/**
 * Given a $level return calling function info at such stack's position
 */
function lgp_get_message_context($level = 3) {
  $stack = lgp_backtrace();
  $frame = $stack[$level];
  return sprintf("%s\nfunction %s() in %s on line %s\n", date("Y-m-d h:i:s"), $frame['function'], $frame['file'], $frame['line']);
}

/**
 * Call LGP's temp_dir_func.
 */
function lgp_get_temp_dir() {
  $temp_dir_func = variable_get('lgp_temp_dir_func', 'lgp_temp_dir');
  return $temp_dir_func();
}

/**
 * Log given message to LGP file.
 */
function lgp_print($message, $use_stdout = FALSE) {
  if (!empty($message)) {
    if ($use_stdout) {
      print $message;
      return;
    }
    $temp = lgp_get_temp_dir();
    $filepath = $temp . '/' . LGP_LOG_FILENAME;
    if (
      is_writable($filepath) ||
      (!file_exists($filepath) && is_writable($temp))
    ) {
      $handle = fopen($filepath, 'a');
      ob_start();
      print($message);
      $output = sprintf("-- %s%s----", lgp_get_message_context(), ob_get_contents());
      ob_end_clean();
      fwrite($handle, $output);
      fwrite($handle, "\n");
      fclose($handle);
    }
    else {
      drupal_set_message('LGP: ' . t("Can't log to file '!path'.", array('!path' => $filepath)), 'error');
    }
  }
}

/**
 * Log to LGP file in print_r format.
 *
 * @param $var
 * @param $keys_only
 *   If $var is object or array, log its keys/properties instead.
 */
function lp($var, $keys_only = FALSE, $use_stdout = FALSE) {
  if ($keys_only) {
    if (is_array($var) || is_object($var)) {
      $keys = array();
      foreach ($var as $key => $val) {
        $keys[$key] = gettype($val);
      }
    }
    if (is_object($var)) {
      $var = (object)$keys; // cast to stdClass()
    }
    elseif (is_array($var)) {
      $var = $keys;
    }
  }
  $message = print_r($var, TRUE);
  $footer = '';
  if (!is_object($var) && !is_array($var)) {
    $footer = "\n";
  }
  lgp_print($message . $footer, $use_stdout);
}

function lfp() {
  $args = func_get_args();
  return call_user_func_array('lp', $args);
}

/**
 * Log to LGP file in var_dump format.
 */
function ld($var, $use_stdout = FALSE) {
  ob_start();
  var_dump($var);
  $message = ob_get_contents();
  ob_end_clean();
  // Log message once buffer is erased and buffering turned off
  lgp_print($message, $use_stdout);
}

function lfd() {
  $args = func_get_args();
  return call_user_func_array('ld', $args);
}

/**
 * Log to LGP file in var_export format.
 */
function lx($var, $use_stdout = FALSE) {
  ob_start();
  var_export($var);
  $message = ob_get_contents();
  ob_end_clean();
  // Log message once buffer is erased and buffering turned off
  lgp_print($message . "\n", $use_stdout);
}

function lfx() {
  $args = func_get_args();
  return call_user_func_array('lx', $args);
}

/**
 * Log backtrace to LGP file without functions arguments.
 */
function lbt($ignore_args = TRUE, $use_stdout = FALSE) {
  $stack = lgp_backtrace($ignore_args);
  array_shift($stack); // Remove me from backtrace
  $message = print_r($stack, TRUE);
  lgp_print($message . "\n", $use_stdout);
}
